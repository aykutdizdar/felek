# Atlassian app password : WXTGfgPSksDszeytjghj

from audioop import reverse
import os
import os.path
import sys
import webbrowser
import pandas
import json
import numpy
from datetime import datetime, timedelta

from googleapiclient.discovery import build
from httplib2 import Http
from configuration import configuration
from oauth2client import client
from oauth2client import file
from oauth2client import tools

from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn import metrics

def read_configuration():
    with open("config.json", "r") as f:
        configuration_file = json.loads(f.read())
    config = configuration(configuration_file["scope"], 
        configuration_file["user_agent"], 
        configuration_file["spreadsheet_id"],
        configuration_file["local_data_file"],
        configuration_file["last_data_download"])
    return config

def write_configuration(config):
    file = open('config.json', 'r')
    json_data = json.load(file)
    file.close()
    
    json_data["last_data_download"] = config.last_data_download
    file = open('config.json', 'w')
    json.dump(json_data, file, indent=2)
    file.close()

def OAuth2Login(config):
    storage = file.Storage(os.path.join(os.path.expanduser('./'), 'token.json'))
    credentials = storage.get()
    if credentials is None or credentials.invalid:
        flow = client.flow_from_clientsecrets("credentials.json", scope=config.scope, redirect_uri='urn:ietf:wg:oauth:2.0:oob')
        uri = flow.step1_get_authorize_url()
        webbrowser.open(uri)
        code = input('Enter the authentication code: ').strip()
        credentials = flow.step2_exchange(code)
        storage.put(credentials)

    if (credentials.token_expiry - datetime.utcnow()) < timedelta(minutes=5):
        credentials.refresh(credentials.authorize(Http()))

    service = build('sheets', 'v4', credentials=credentials, static_discovery=False)
    return service

def get_sheet_data(service, spreadsheet_id):

    raise Exception('Not fully implemented')

    # Retrieve the documents contents from the Docs service.
    spreadsheet = service.spreadsheets().get(spreadsheetId = spreadsheet_id).execute()
    print('The title of the spreadsheet is: {}'.format(spreadsheet["properties"]["title"]))
    for sheet in spreadsheet.get("sheets"):
        if 'title' in sheet['properties'].keys():
            if sheet['properties']['title'] == "Data":
                print("Sheet found : {0}".format(sheet['properties']['title']))
                break
    
    print("Sheet selected :".format(sheet["properties"]["title"]))
    return sheet

def shitty_way_to_download_data(spreadsheet_id, sheet_id, save_to_file):
    uri = f'https://docs.google.com/spreadsheets/d/e/2PACX-1vT6K3J_VSRlePYIsDEglkqL4_IHhYfA4qbls1MoL_CXzu1aObhF_HU4lkRE0EHltli1vjaEjfrOlZck/pub?gid=0&single=true&output=csv'

    # Omit first row
    df = pandas.read_csv(uri, header=1)
    
    # Set dummy variables
    # df = df.replace('x', 1.0)
    df = df.fillna(0.0)

    df.to_csv(save_to_file)

def merge(list1, list2):
    merged_list = tuple(zip(list1, list2)) 
    return merged_list

def load_data_from_file(file_name):
    return pandas.read_csv(file_name)

def multiple_regression(df):
    results = {}

    # Indices of input variable (independent) boundaries
    idx_first_x_row = 0
    idx_first_x_col = 2
    idx_last_x_row = None
    idx_last_x_col = None

    # Indices of output variable (dependent) boundaries
    idx_first_y_row = 0
    idx_first_y_col = None
    idx_last_y_row = None
    idx_last_y_col = len(df.columns)

    today = datetime.today().strftime("%d.%m.%Y")

    idx_last_x_col = df.columns.get_loc("#") 
    idx_first_y_col = df.columns.get_loc("#") + 1
    for index, data in df.iterrows():
        if(data[1] == today):
            idx_last_x_row = int(index) + 1
            idx_last_y_row = int(index) + 1
            break

    x = df.iloc[idx_first_x_row:idx_last_x_row,idx_first_x_col:idx_last_x_col]
    y = df.iloc[idx_first_y_row:idx_last_y_row,idx_first_y_col:idx_last_y_col]

    # Factorize
    for label, item in x.iteritems():
        for j in range(len(item) - 1, 0, -1):
            if(j > 0 and int(item[j-1]) > 0):
                item[j] += int(item[j-1]) * 0.50

    for y_label, y_content in y.iteritems():
        #print("-----")
        #print(y_label)

        mlr = LinearRegression(fit_intercept = False)  
        mlr.fit(x.values, y_content.values)
        
        intercept = mlr.intercept_
        coef = merge(x.columns.values, list(numpy.round(mlr.coef_, 1)))
        r2 = mlr.score(x.values, y_content.values)

        #print("Intercept: {:.2f}".format(intercept))
        #print("Coefficients:")
        #print(coef)
        #print('R squared: {:.2f}'.format(r2))
        
        results[y_label] = [ intercept, coef, r2 ]

    return results

def get_sheet(spreadsheet, name):
    sheet_out = None
    for sheet in spreadsheet.get("sheets"):
        if 'title' in sheet['properties'].keys():
            if sheet['properties']['title'] == name:
                sheet_out = sheet
                break
    return sheet_out

def create_sheet(service, spreadsheet_id, sheet_title):
    request_body = {
                'requests': [{
                        'addSheet': {
                            'properties': {
                                'title': sheet_title
                            }
                        }
                    }
                ]}
    request = service.spreadsheets().batchUpdate(spreadsheetId=spreadsheet_id, body=request_body)
    return request.execute()
    
def clear_sheet(service, spreadsheet_id, sheet_id):
    request_body = {
        "requests": [
            {
            "updateCells": {
                "range": {
                "sheetId": sheet_id
                },
                "fields": "userEnteredValue"
            }
            }]
        }
    request = service.spreadsheets().batchUpdate(spreadsheetId=spreadsheet_id, body=request_body)
    return request.execute()

def upload_results(service, spreadsheet_id, results, sheet_title):
    spreadsheet = service.spreadsheets().get(spreadsheetId=spreadsheet_id).execute()
    print('Found spreadsheet: {}'.format(spreadsheet["properties"]["title"]))

    sheet = get_sheet(spreadsheet, sheet_title)
    print('Found sheet: {0} [{1}]'.format(sheet["properties"]["title"], sheet["properties"]["sheetId"]))

    if sheet == None:
        print("Couldn't find sheet Creating...")
        create_sheet(service, spreadsheet_id, sheet_title)
        print("Sheet created.")
        sheet = get_sheet(spreadsheet, sheet_title)
        print('Found sheet: {0} [{1}]'.format(sheet["properties"]["title"], sheet["properties"]["sheetId"]))

    # Reformat results
    values = []
    values.append(["Output", "Intercept", "R2", "First", "Second", "Third"])
    for res_label in results:
        # results[res_label][1] tuple'ini abs ikinci değere göre sırala
        sorted_vals = sorted(results[res_label][1], key = lambda x: abs(x[1]), reverse=True)
        values.append([res_label, "{:.2f}".format(results[res_label][0]), "{:.2f}".format(results[res_label][2]), 
            "{0}:{1}".format(sorted_vals[0][1], sorted_vals[0][0]),
            "{0}:{1}".format(sorted_vals[1][1], sorted_vals[1][0]),
            "{0}:{1}".format(sorted_vals[2][1], sorted_vals[2][0]),
            ])

    request_body = {
        "valueInputOption": "RAW",
        "data": [
            {
                "range": "Correlation!A1:Z100",
                "majorDimension": "ROWS",
                "values": values, 
            }
        ],
        "includeValuesInResponse": "false",
        "responseValueRenderOption": "UNFORMATTED_VALUE",
        "responseDateTimeRenderOption": "FORMATTED_STRING"
    }

    request = service.spreadsheets().values().batchUpdate(spreadsheetId=spreadsheet_id, body=request_body)
    return request.execute()

def main():
    print("Begin")
    print(sys.version)

    # Konfigürasyonu başlat
    config = read_configuration()
    
    # Google servislerine yanaş
    service = OAuth2Login(config)

    # Eğer son kontrolün üzerinden 1 günden fazla geçtiyse
    # Data sayfasını CSV olarak download et ve sakla 
    if( int(datetime.today().strftime('%Y%m%d')) > int(config.last_data_download)):
        print("Document expired. Redownloading...")
        shitty_way_to_download_data(config.spreadsheet_id, "Data", config.local_data_file)
        
        config.last_data_download = datetime.strftime(datetime.today(), "%Y%m%d")
        write_configuration(config)
        print("Document downloaded and saved")

    # Diskten geri getirelim
    df = load_data_from_file(config.local_data_file)

    # Multiple regression for dummies
    print("Executing regression...")
    results = multiple_regression(df)

    # Cloud'a yükleyelim
    print("Uploading to Google...")
    upload_results(service, config.spreadsheet_id, results, "Correlation")
    print("Results uploaded.")

    print("Done")

if __name__ == "__main__":
    main()