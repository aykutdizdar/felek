class configuration:
    def __init__(self, scope = None, user_agent = None, spreadsheet_id = None, local_data_file = None, last_data_download = None):
        self.scope = scope
        self.user_agent = user_agent
        self.spreadsheet_id = spreadsheet_id
        self.local_data_file = local_data_file
        self.last_data_download = last_data_download
